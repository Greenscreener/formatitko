from panflute import *
import re

# Import local files
from whitespace import *
from command import *
from util import *
from context import *


# This is a small extension to the Quoted panflute elements which allows to
# have language-aware quotation marks.
class FQuoted(Quoted):
	def __init__(self, *args, **kwargs):
		self.style = kwargs["style"]
		del kwargs["style"]
		super().__init__(*args, **kwargs)


# This is where tha magic happens. This function transforms a single element,
# to transform the entire tree, panflute's walk should be used.
def transform(e: Element, c: Context) -> Element:

	# Determine if this space should be non-breakable. See whitespace.py.
	if isinstance(e, Whitespace) and bavlna(e, c):
		e = NBSP()

	if hasattr(e, "attributes"):
		# `if` attribute. Only show this element if flag is set.
		if "if" in e.attributes:
			if not c.is_flag_set(e.attributes["if"]):
				return nullify(e)
		# `ifn` attribute. Only show this element if flag is NOT set
		if "ifn" in e.attributes:
			if c.is_flag_set(e.attributes["ifn"]):
				return nullify(e)

		# There are multiple ways to call a command so we turn it into a
		# unified element first and then call it at the end. This handles the
		# []{c=commandname} and
		# :::{c=commandname}
		# :::
		# syntax.
		if (isinstance(e, Div) or isinstance(e, Span)) and "c" in e.attributes:
			if isinstance(e, Div):
				e = BlockCommand(*e.content, identifier=e.identifier, classes=e.classes, attributes=e.attributes)
			else:
				e = InlineCommand(*e.content, identifier=e.identifier, classes=e.classes, attributes=e.attributes)

		# Isolated subdocuments using Group and a different Context.  Can be
		# separate files (using attribute `partial`) or be inline using the
		# following syntax:
		# ```markdown {.group}
		# * file content *
		# ```
		# Both can contain their own metadata in a FrontMatter (YAML header)
		if (isinstance(e, Div) and "partial" in e.attributes)\
		  or (isinstance(e, CodeBlock) and "markdown" in e.classes and "group" in e.classes):
			if isinstance(e, Div):
				if not c.trusted: # If we're in an untrusted context, we shouldn't allow inclusion of files outside the PWD.
					full_path = os.path.abspath(c.dir + "/" + e.attributes["partial"])
					pwd = os.path.abspath(".")
					if os.path.commonpath([full_path, pwd]) != os.path.commonpath([pwd]):
						return nullify(e)
				text = open(c.dir + "/" + e.attributes["partial"], "r").read()
				path = c.dir + "/" + e.attributes["partial"]
			else:
				text = e.text
				path = c.path
			if "type" in e.attributes and e.attributes["type"] in ["tex", "html"]:
				e = RawBlock(text, e.attributes["type"])
			else:
				includedDoc = import_md(text)
				trusted = True
				if "untrusted" in e.attributes and (e.attributes["untrusted"] == True or e.attributes["untrusted"] == 'True'):
					trusted = False
				if not c.trusted:
					trusted = False
				nContext = Context(includedDoc, path, c, trusted=trusted)
				language = includedDoc.get_metadata("language")
				includedDoc = includedDoc.walk(transform, nContext)
				e = Group(*includedDoc.content, metadata={"language": language})

	# Transform panflute's Quoted to custom FQuoted, see above.
	if isinstance(e, Quoted):
		quote_styles = {
			"cs": "cs",
			"en": "en",
			"sk": "cs",
			None: None
		}
		e = FQuoted(*e.content, quote_type=e.quote_type, style=quote_styles[c.get_metadata("language")])
	
	if isinstance(e, Image):
		# Pass down the directory of the current source file for finding image
		# files.
		e.attributes["source_dir"] = c.dir
		# Pass down "no-srcset" metadatum as attribute down to images.
		if not "no-srcset" in e.attributes:
			e.attributes["no-srcset"] = c.get_metadata("no-srcset") if c.get_metadata("no-srcset") is not None else False

	# Pass down metadata 'highlight' and 'highlight_style' as attribute to CodeBlocks
	if isinstance(e, CodeBlock):
		if not "highlight" in e.attributes:
			e.attributes["highlight"] = c.get_metadata("highlight") if c.get_metadata("highlight") is not None else True
		if not "style" in e.attributes:
			e.attributes["style"] = c.get_metadata("highlight-style") if c.get_metadata("highlight-style") is not None else "default"
			e.attributes["noclasses"] = False
		else:
			e.attributes["noclasses"] = True

	# Execute python code inside source code block. Works the same as commands.
	# Syntax:
	# ```python {.run}
	# print("woo")
	# ```
	if isinstance(e, CodeBlock) and hasattr(e, "classes") and "python" in e.classes and "run" in e.classes:
		if not c.trusted:
			return nullify(e)
		e = Div(*executeCommand(e.text, None, c))
		e = e.walk(transform, c)

	# Command defines for calling using BlockCommand and InlineCommand. If
	# redefine is used instead of define, the program doesn't check if the
	# command already exists.
	# Syntax:
	# ```python {define=commandname}
	# print(wooo)
	# ```
	if isinstance(e, CodeBlock) and hasattr(e, "classes") and "python" in e.classes and hasattr(e, "attributes")\
		and ("define" in e.attributes or "redefine" in e.attributes):
		if not c.trusted:
			return nullify(e)
		e = handle_command_define(e, c)

	## Shorthands
	# Shorter (and sometimes the only) forms of certain features
	if isinstance(e, Span) and len(e.content) == 1 and isinstance(e.content[0], Str):
		## Handle special command shorthand [!commandname]{}
		if re.match(r"^![\w]+$", e.content[0].text):
			e = InlineCommand(identifier=e.identifier, classes=e.classes, attributes={**e.attributes, "c": e.content[0].text[1:]})

		## Handle import [#path/file.md]{}
		# This is the exact opposite of partials. We take the commands, flags
		# and metadata but drop the content.
		elif re.match(r"^#.+$", e.content[0].text):
			importedDoc = import_md(open(c.dir + "/" + e.content[0].text[1:], "r").read())
			importedDoc.walk(transform, c)
			return nullify(e)

		## Handle metadata print [$key1.key2]{}
		# This is a shorthand for just printing the content of some metadata.
		elif re.match(r"^\$[\w.]+$", e.content[0].text):
			val = c.get_metadata(e.content[0].text[1:], False)
			if isinstance(val, MetaInlines):
				e = Span(*val.content)
				e = e.walk(transform, c)
			elif isinstance(val, MetaString):
				e = Span(Str(val.string))
			elif isinstance(val, MetaBool):
				e = Span(Str(str(val.boolean)))
			else:
				raise TypeError(f"Cannot print value of metadatum '{e.content[0].text[1:]}' of type '{type(val)}'")

	## Execute commands
	# panflute's walk function transforms the children first, then the root
	# element, so the content the command receives is already transformed.
	# The output from the command is then transformed manually again.
	if isinstance(e, Command):
		if not c.get_command(e.attributes["c"]):
			raise NameError(f"Command not defined '{e.attributes['c']}'.")
		e = e.replaceSelf(executeCommand(c.get_command(e.attributes["c"]), e, c))
		e = e.walk(transform, c)

	return e
