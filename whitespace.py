from panflute import Space, SoftBreak, Str, Math
from typing import Union

# Import local files
from context import Context

Whitespace = Union[Space,SoftBreak]

class NBSP(Space):
	pass

# This function tries to determine if a space should be non-breaking. It is
# language-aware and tries to be sort-of smart about its decisions.
def bavlna(e: Whitespace, c: Context) -> bool:
		
	if c.get_metadata("language") == "cs":
		# Add no-break space after single letter prepositions and conjunctions.
		# Also tries to find them inside elements, for instance
		# `V [odevzdávátku]()` should get correctly detected.
		prev = e.prev if isinstance(e.prev, Str) else (e.prev.content[-1] if hasattr(e.prev, "content") and len(e.prev.content) != 0 else None)
		next = e.next if isinstance(e.next, Str) else (e.next.content[0] if hasattr(e.next, "content") and len(e.next.content) != 0 else None)
		if isinstance(prev, Str) and isinstance(next, Str):
			if prev.text.lower() in ['k', 's', 'v', 'z', 'o', 'u', 'a', 'i']:
				return True
			
	if isinstance(e.prev, Str) and isinstance(e.next, Str):
		# Add no-break space between numbers or numbers and operators.
		prevC = e.prev.text[-1]
		nextC = e.next.text[0]
		numbers = ["0123456789"]
		operators = ["+-/*^%:"]
		if prevC in numbers and nextC in numbers:
			return True
		if prevC in numbers and nextC in operators:
			return True
		if prevC in operators and nextC in numbers:
			return True

	if isinstance(e.prev, Math) or isinstance(e.next, Math):
		# Add no-break spaces around TeX math.
		return True



	return False

