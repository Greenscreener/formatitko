#!/usr/bin/env python3

import argparse
import re
import sys
from typing import List
import os

# Import local files
from transform import transform
from util import *
from context import Context, Group
from katex import KatexClient
from html import html
from tex import tex
from images import ImageProcessor

from mj_show import show

# Initialize command line arguments
parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-l", "--img-lookup-dirs", help="Image lookup directories. When processing images, the program will try to find the image in them first. Always looks for images in the same folder as the markdown file.", nargs="+", default=[])
parser.add_argument("-p", "--img-public-dir", help="Directory to put processed images into. The program will not overwrite existing images.", default="public")
parser.add_argument("-i", "--img-web-path", help="Path where the processed images are available on the website.", default="/")
parser.add_argument("-w", "--output-html", help="The HTML file (for Web) to write into.", default="output.html")
parser.add_argument("-t", "--output-tex", help="The TEX file to write into.", default="output.tex")
parser.add_argument("input_filename", help="The markdown file to process.")
parser.add_argument("--debug", action='store_true')
args = parser.parse_args()
# TODO: Accept path to unix socket for katexClient, then don't init our own,
# just connect to an existing one. For formátíking many files in a row.

# Use panflute to parse the input MD file
doc = import_md(open(args.input_filename, "r").read())

if args.debug:
	print(show(doc))

# The language metadatum is important, so it's read before transformation and
# then attached to a group inside the Doc
language = doc.get_metadata("language", None, True)
context = Context(doc, args.input_filename)

# Transform the document. This includes all the fancy formatting this software does.
doc = doc.walk(transform, context)

# Now wrap the document contents in a group, which is able to pop its language
# setting out to TeX
doc.content = [Group(*doc.content, metadata={"language":language})]

# Initialize the image processor (this just keeps some basic state)
imageProcessor = ImageProcessor(args.img_public_dir, args.img_web_path, *args.img_lookup_dirs)

# Initialize KaTeX client (this runs the node app and connects to a unix socket)
with KatexClient() as katexClient:
	# Generate HTML and TeX out of the transformed document
	open(args.output_html, "w").write(html(doc, katexClient, imageProcessor))
	open(args.output_tex, "w").write(tex(doc, imageProcessor))

if args.debug:
	print(show(doc))

