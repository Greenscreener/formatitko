###
# For debugging, borrowed from here: git://git.ucw.cz/labsconf2022.git
###

import sys, re
from panflute import *

avoid_keys = {
	'dict',
	'list',
	'location',
	'oktypes',
	'parent',
}

def show(e, file=sys.stdout, indent=""):
	t = re.sub(r".*\.(.*)'>", r'\1', str(type(e)))
	if isinstance(e, Str):
		file.write(f'{indent}{t} "{e.text}"\n')
	else:
		file.write(f'{indent}{t}\n')

	if not isinstance(e, Str):
		for k in e.__slots__:
			if hasattr(e, k) and not k.startswith('_') and not k in avoid_keys:
				file.write(f'{indent}    {k}={getattr(e, k)}\n')

	if isinstance(e, Element):
		children = sorted((child_name, getattr(e, child_name)) for child_name in e._children)
		for name, c in children:
			if name == 'content':
				show(c, file, indent + '    ')
			elif c is not None:
				file.write(f'{indent}    {name}=\n')
				show(c, file, indent + '        ')
			else:
				file.write(f'{indent}    {name}=None\n')
	elif isinstance(e, ListContainer):
		for c in e:
			show(c, file, indent + '    ')
	elif isinstance(e, DictContainer):
		for name, c in e.items():
			file.write(f'{indent}    {name}:\n')
			show(c, file, indent + '        ')
	else:
		raise TypeError(type(e))

