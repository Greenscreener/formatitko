---
title: A subfile!
---
I am a little piece of content

# With a title!

And things...

:::{if=cat}

:::

``` {.python .run}
# I set my own flags!
ctx.set_flag("cat", True)
```

``` {.python .run}
println(f"The subdocument's title is \n\n# {ctx.get_metadata('title')}")
println()
println(f"The subdocument's subtitle is \n\n## {ctx.get_metadata('subtitle')}")
```

```markdown {.group}
---
language: "cs"
---
Tak toto je "v prádelně" pánové!
```

```markdown {.group}
---
language: "en"
---
This is "in a laundry room" gentlemen!
```

I am a duck.

:::{if=cat}
This should be only shown to included cats.
:::


$$
\def\eqalign#1{NO, just, nooooo}
\eqalign{}
$$


![This is a figure, go figure...](logo.svg){width=25%}

![This is a figure, go figure...](logo.pdf){width=50%}

![This is a figure, go figure...](logo.jpg){width=50%}

![This is a figure, go figure...](logo1.png){width=10em}

![Fakt epesní reproduktor](reproduktor.jpeg){width=10em}

![Fakt epesní reproduktor](reproduktor.png "Hodně rozpixelovaný obrázek reproduktoru"){width=10em file-width=1000}

