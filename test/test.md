---
title: 'Wooooo a title'
subtitle: 'A subtitle'
are_we_there_yet: False
language: "en"
---
[#test-files/test-import.md]{}

# Hello world!

This is an *example* **yay**!

This is *very **strongly** emphasised*

Příliš žluťoučký kůň pěl dábelské ódy. *Příliš žluťoučký kůň pěl dábelské ódy.* **Příliš žluťoučký kůň pěl dábelské ódy.** ***Příliš žluťoučký kůň pěl dábelské ódy.***

:::{partial=test-files/evil.md untrusted=True}
:::

:::{partial=test-files/test-partial.md}
:::

:::{if=cat}
This should only be shown to cats
:::

```python {.run}
ctx.set_flag("cat", True)
```

```python {.run}
println(f"The main document's title is '{ctx.get_metadata('title')}'")
ctx.set_metadata("a", {})
ctx.set_metadata("a.b", {})
ctx.set_metadata("a.b.c", "Bruh **bruh** bruh")
```

```python {style=native}
def bruh(no):
    wat
```

Inline `code`

::::{if=cat}
This should only be shown to cats the second time
::::

# [$are_we_there_yet]{}

```markdown {.group}
---
language: cs
---
V&#8203; pravém jízdním bruhu.
V pravém jízdním bruhu.
V pravém jízdním bruhu.
V&nbsp;pravém jízdním bruhu.

[!opendatatask]{}
```

[This too!]{if=cat}

[What]{.co}

[An inline command with contents and **bold** and another [!nop]{} inside!]{c=nop}

[!nop]{a=b}<!-- A special command! WOW -->

> OOO a blockquote mate init
>
>> Nesting??
>> Woah

A non-breakable&nbsp;space bro

A lot              of spaces

A text with some inline math: $\sum_{i=1}^nn^2$. Plus some display math:

A link with the link in the link: <https://bruh.com>

H~2~O is a liquid.  2^10^ is 1024.

[Underline]{.underline}

:::{only=html}
$$
\def\eqalign#1{\begin{align*}#1\end{align*}}
$$
:::

$$
\eqalign{
           2 x_2 + 6 x_3 &= 14 \cr
     x_1 - 3 x_2 + 2 x_3 &= 5 \cr
    -x_1 + 4 x_2 + \phantom{1} x_3 &= 2
}
$$

:::{partial=test-files/test-partial.md}
:::

---

This should be seen by all.^[This is a footnote]

| Matematicko-fyzikální fakulta University Karlovy
| Malostranské nám. 2/25
| 118 00 Praha 1

More footnotes.^[I am a foot]

To Do:

- buy eggs
- buy milk
- ???
- profit
    - also create sublists preferrably

1. Woah
2. Wooo
3. no

4) WOO

``` {=html}
<figure>
    <video src="woah.mp4" autoplay></video>
    <figcaption> This is indeed a video </figcaption>
</figure>
```

#. brum
#. BRUHHH
#. woah

i. bro
ii. wym bro


+---------------------+-----------------------+
| Location            | Temperature 1961-1990 |
|                     | in degree Celsius     |
+---------------------+-------+-------+-------+
|                     | min   | mean  | max   |
+=====================+=======+=======+======:+
| Antarctica          | -89.2 | N/A   | 19.8  |
+---------------------+-------+-------+-------+
| Earth               | -89.2 | 14    | 56.7  |
+---------------------+-------+-------+-------+

-------     ------ ----------   -------
     12     12        12             12
    123     123       123           123
      1     1          1        1
-------     ------ ----------   -------

