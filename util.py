from panflute import Element, Block, Inline, Null, Str, Doc, convert_text, Para, Plain
import re

# It sometimes happens that an element contains a single paragraph or even a
# single plaintext line. It can be sometimes useful to extract this single
# paragraph, which is inline.
def inlinify(e: Element) -> Element:
	if len(e.content) == 1 and (isinstance(e.content[0], Para) or isinstance(e.content[0], Plain)):
		return e.content[0].content

# In transform, inline elements cannot be replaced with Block ones and also
# cannot be removed from the tree entirely, because that would mess up the
# iteration process through the tree. We replace them with null elements
# instead which never make it to the output.
def nullify(e: Element):
	if isinstance(e, Inline):
		return Str("")
	elif isinstance(e, Block):
		return Null()

# A helper function to import markdown using panflute (which calls pandoc). If
# we ever want to disable or enable some of panflute's markdown extensions,
# this is the place to do it.
def import_md(s: str, standalone: bool=True) -> Doc:
	return convert_text(s, standalone=standalone, input_format="markdown-definition_lists-citations")
